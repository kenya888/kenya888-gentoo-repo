# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

CRATES="
	adler-1.0.2
	ahash-0.7.6
	android_system_properties-0.1.5
	anyhow-1.0.66
	atom_syndication-0.11.0
	atty-0.2.14
	autocfg-1.1.0
	base64-0.13.1
	bitflags-1.3.2
	block-0.1.6
	bumpalo-3.11.1
	bytes-1.2.1
	cassowary-0.3.0
	cc-1.0.76
	cesu8-1.1.0
	cfg-if-1.0.0
	chrono-0.4.22
	chunked_transfer-1.4.0
	clap-4.0.26
	clap_derive-4.0.21
	clap_lex-0.3.0
	clipboard-win-3.1.1
	codespan-reporting-0.11.1
	combine-4.6.6
	copypasta-0.8.1
	core-foundation-sys-0.8.3
	crc32fast-1.3.2
	crossterm-0.25.0
	crossterm_winapi-0.9.0
	cty-0.2.2
	cxx-1.0.81
	cxx-build-1.0.81
	cxxbridge-flags-1.0.81
	cxxbridge-macro-1.0.81
	diligent-date-parser-0.1.3
	directories-4.0.1
	dirs-sys-0.3.7
	dlib-0.5.0
	downcast-rs-1.2.0
	encoding_rs-0.8.31
	fallible-iterator-0.2.0
	fallible-streaming-iterator-0.1.9
	flate2-1.0.24
	form_urlencoded-1.1.0
	futf-0.1.5
	getrandom-0.2.8
	hashbrown-0.12.3
	hashlink-0.8.1
	heck-0.4.0
	hermit-abi-0.1.19
	html2text-0.4.4
	html5ever-0.26.0
	iana-time-zone-0.1.53
	iana-time-zone-haiku-0.1.1
	idna-0.3.0
	jni-0.20.0
	jni-sys-0.3.0
	js-sys-0.3.60
	lazy-bytes-cast-5.0.1
	lazy_static-1.4.0
	libc-0.2.137
	libloading-0.7.4
	libsqlite3-sys-0.25.2
	link-cplusplus-1.0.7
	lock_api-0.4.9
	log-0.4.17
	mac-0.1.1
	malloc_buf-0.0.6
	markup5ever-0.11.0
	memchr-2.5.0
	memmap2-0.5.8
	memoffset-0.6.5
	minimal-lexical-0.2.1
	miniz_oxide-0.5.4
	mio-0.8.5
	ndk-context-0.1.1
	new_debug_unreachable-1.0.4
	nix-0.24.2
	nom-7.1.1
	num-integer-0.1.45
	num-traits-0.2.15
	num_cpus-1.14.0
	objc-0.2.7
	objc-foundation-0.1.1
	objc_id-0.1.1
	once_cell-1.16.0
	os_str_bytes-6.3.1
	parking_lot-0.12.1
	parking_lot_core-0.9.4
	percent-encoding-2.2.0
	phf-0.10.1
	phf_codegen-0.10.0
	phf_generator-0.10.0
	phf_shared-0.10.0
	pkg-config-0.3.26
	ppv-lite86-0.2.17
	precomputed-hash-0.1.1
	proc-macro-error-1.0.4
	proc-macro-error-attr-1.0.4
	proc-macro2-1.0.47
	quick-xml-0.22.0
	quote-1.0.21
	r2d2-0.8.10
	r2d2_sqlite-0.21.0
	rand-0.8.5
	rand_chacha-0.3.1
	rand_core-0.6.4
	raw-window-handle-0.5.0
	redox_syscall-0.2.16
	redox_users-0.4.3
	ring-0.16.20
	rss-2.0.1
	rusqlite-0.28.0
	rustls-0.20.7
	same-file-1.0.6
	scheduled-thread-pool-0.2.6
	scoped-tls-1.0.1
	scopeguard-1.1.0
	scratch-1.0.2
	sct-0.7.0
	serde-1.0.147
	signal-hook-0.3.14
	signal-hook-mio-0.2.3
	signal-hook-registry-1.4.0
	siphasher-0.3.10
	smallvec-1.10.0
	smithay-client-toolkit-0.16.0
	smithay-clipboard-0.6.6
	spin-0.5.2
	string_cache-0.8.4
	string_cache_codegen-0.5.2
	strsim-0.10.0
	syn-1.0.103
	tendril-0.4.3
	termcolor-1.1.3
	thiserror-1.0.37
	thiserror-impl-1.0.37
	tinyvec-1.6.0
	tinyvec_macros-0.1.0
	tui-0.19.0
	unicode-bidi-0.3.8
	unicode-ident-1.0.5
	unicode-normalization-0.1.22
	unicode-segmentation-1.10.0
	unicode-width-0.1.10
	untrusted-0.7.1
	ureq-2.5.0
	url-2.3.1
	utf-8-0.7.6
	vcpkg-0.2.15
	version_check-0.9.4
	walkdir-2.3.2
	wasi-0.11.0+wasi-snapshot-preview1
	wasm-bindgen-0.2.83
	wasm-bindgen-backend-0.2.83
	wasm-bindgen-macro-0.2.83
	wasm-bindgen-macro-support-0.2.83
	wasm-bindgen-shared-0.2.83
	wayland-client-0.29.5
	wayland-commons-0.29.5
	wayland-cursor-0.29.5
	wayland-protocols-0.29.5
	wayland-scanner-0.29.5
	wayland-sys-0.29.5
	web-sys-0.3.60
	webbrowser-0.8.2
	webpki-0.22.0
	webpki-roots-0.22.5
	widestring-1.0.2
	winapi-0.3.9
	winapi-i686-pc-windows-gnu-0.4.0
	winapi-util-0.1.5
	winapi-x86_64-pc-windows-gnu-0.4.0
	windows-sys-0.42.0
	windows_aarch64_gnullvm-0.42.0
	windows_aarch64_msvc-0.42.0
	windows_i686_gnu-0.42.0
	windows_i686_msvc-0.42.0
	windows_x86_64_gnu-0.42.0
	windows_x86_64_gnullvm-0.42.0
	windows_x86_64_msvc-0.42.0
	wsl-0.1.0
	x11-clipboard-0.6.1
	xcb-1.2.0
	xcursor-0.3.4
	xml-rs-0.8.4
	xml5ever-0.17.0
"

inherit cargo

DESCRIPTION="A TUI RSS/Atom reader with vim-like controls and a local-first, offline-first focus."
HOMEPAGE="https://github.com/ckampfe/russ"
GIT_COMMIT="4648f52847b4f90f1c1eb367efccc143c4dca589"
SRC_URI="https://github.com/ckampfe/russ/archive/${GIT_COMMIT}.tar.gz -> ${P}.tar.gz $(cargo_crate_uris)"

LICENSE="Apache-2.0 MIT 0BSD Apache-2.0-with-LLVM-exceptions BSD Boost-1.0 ISC MIT MPL-2.0 Unicode-DFS-2016 Unlicense ZLIB"
SLOT="0"
KEYWORDS="~amd64"
IUSE="system-sqlite"

COMMON_DEPEND="
	system-sqlite? ( dev-db/sqlite )
"
BDEPEND="dev-lang/rust
	${COMMON_DEPEND}
"
DEPEND="
	${COMMON_DEPEND}
"
pkg_pretend() {
	if use system-sqlite;then
		ewarn ""
		ewarn "The system-sqlite USE flag is set."
		ewarn "It is not supported by the upstream project."
		ewarn "https://github.com/ckampfe/russ"
		ewarn "DO NOT report issues to upstream about system-sqlite related issue."
		ewarn ""
	fi
	sleep 5
}

src_unpack() {
	S="${WORKDIR}/${PN}-${GIT_COMMIT}"
	cargo_src_unpack
}

src_prepare() {
	local PATCHES=(
	)

	if use system-sqlite; then
		PATCHES+=(
		"${FILESDIR}/0001-use-system-sqlite.patch"
		)
	fi

	default
}
