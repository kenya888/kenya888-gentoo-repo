# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Write Fedora Images to Portable Media"
HOMEPAGE="https://github.com/FedoraQt/MediaWriter"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="
	app-arch/xz-utils
	dev-qt/qtdeclarative[widgets]
	dev-qt/qtnetwork
	dev-qt/qtdbus
"

RDEPEND="${DEPEND}
"

RESTRICT="test"

inherit multilib qmake-utils


if [[ ${PV} == *9999* ]]; then
	inherit git-r3
	EGIT_REPO_URI="${HOMEPAGE}.git"

else
	SRC_URI="${HOMEPAGE}/archive/${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
	S="${WORKDIR}"/MediaWriter-${PV}
fi

src_configure() {
	default
	eqmake5 PREFIX=/usr
}

src_install() {
	emake INSTALL_ROOT="${D}" install
}
