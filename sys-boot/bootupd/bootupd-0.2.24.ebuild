# Copyright 2017-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cargo unpacker systemd

DESCRIPTION="Bootloader updater"
HOMEPAGE="https://github.com/coreos/${PN}"

SRC_URI="https://github.com/coreos/${PN}/releases/download/v${PV}/${P}.crate -> ${P}.tar.gz
https://github.com/coreos/${PN}/releases/download/v${PV}/${P}-vendor.tar.zstd -> ${P}-vendor.tar.zst
"

KEYWORDS="~amd64 ~arm64"

LICENSE="Apache-2.0 Apache-2.0-with-LLVM-exceptions BSD MIT Unicode-DFS-2016"
SLOT="0"

RDEPEND=""
DEPEND="
    app-arch/zstd
	sys-apps/systemd
	${RDEPEND}
"

src_unpack() {
	unpacker
	cargo_src_unpack
	rmdir ${ECARGO_VENDOR} || die
	ln -sf ${WORKDIR}/vendor ${ECARGO_VENDOR} || die
}

src_install() {
	exeinto /usr/libexec
	doexe $(cargo_target_dir)/bootupd
    dosym /usr/libexec/bootupd /usr/bin/bootupctl
	keepdir /usr/$(get_libdir)/bootupd/grub2-static
	insinto /usr/$(get_libdir)/bootupd/grub2-static
	doins ${S}/src/grub2/*.cfg
}
