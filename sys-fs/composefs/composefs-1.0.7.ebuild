# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson

DESCRIPTION="a file system for mounting container images"
HOMEPAGE="https://github.com/containers/composefs"

if [ ${PV} == "9999" ] ; then
    inherit git-r3
    EGIT_REPO_URI="https://github.com/containers/${PN}"
else
    SRC_URI="https://github.com/containers/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
    KEYWORDS="~amd64 ~arm64"
fi

LICENSE="GPL-2+ LGPL-2+ Apache-2.0"

SLOT="0"
KEYWORDS="~amd64 ~arm64"

IUSE=""

BDEPEND="
    virtual/pkgconfig
	dev-go/go-md2man
"
RDEPEND=""
DEPEND="${RDEPEND}
	dev-libs/openssl
"
src_configure() {
	local emesonargs=(
		--default-library=shared -Dfuse=enabled -Dman=enabled
	)
	meson_src_configure
}

src_install() {
	meson_src_install
}
