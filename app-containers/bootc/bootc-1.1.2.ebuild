# Copyright 2017-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cargo unpacker systemd

DESCRIPTION="Boot and upgrade via container images"
HOMEPAGE="https://github.com/containers/${PN}"

SRC_URI="https://github.com/containers/${PN}/releases/download/v${PV}/${P}.tar.zstd -> ${P}.tar.zst
https://github.com/containers/bootc/releases/download/v${PV}/${P}-vendor.tar.zstd -> ${P}-vendor.tar.zst
"

KEYWORDS="~amd64 ~arm64"
QA_FLAGS_IGNORED="usr/bin/bootc"

LICENSE="Apache-2.0 Apache-2.0-with-LLVM-exceptions BSD MIT Unicode-DFS-2016"
SLOT="0"

RDEPEND="
	app-containers/podman
	app-containers/skopeo
	dev-util/ostree
	sys-fs/composefs
"

DEPEND="
    app-arch/zstd
	sys-apps/systemd
	${RDEPEND}
"

src_unpack() {
	unpacker
	cargo_src_unpack
	rmdir ${ECARGO_VENDOR} || die
	ln -sf ${WORKDIR}/vendor ${ECARGO_VENDOR} || die
}

src_install() {
	cargo_src_install --path ./cli
    doman ${S}/man/*
	keepdir /usr/$(get_libdir)/bootc/bound-images,d
	keepdir /usr/$(get_libdir)/bootc/kargs.d
	keepdir /usr/$(get_libdir)/bootc/install
	dosym /sysroot/ostree/bootc/storage /usr/$(get_libdir)/bootc/storage
	dosym /usr/bin/bootc /usr/$(get_libdir)/systemd/system-generators/bootc-systemd-generator
	systemd_dounit ${S}/systemd/*
}
