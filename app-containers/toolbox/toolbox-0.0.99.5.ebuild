# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit bash-completion-r1 meson tmpfiles

DESCRIPTION="A tool for Linux, which allows the use of interactive command line environments for development and troubleshooting the host operating system, without having to install software on the host."
HOMEPAGE="https://github.com/containers/toolbox"
SRC_URI="https://github.com/containers/toolbox/archive/${PV}.tar.gz -> ${P}.tar.gz http://kenya888-gentoo-repo-distfiles.s3.fr-par.scw.cloud/${P}-vendor.tar.gz"

LICENSE="Apache-2.0 BSD BSD-2 CC-BY-SA-4.0 ISC MIT MPL-2.0"
SLOT="0"

BDEPEND="
	dev-lang/go
	dev-go/go-md2man
	dev-build/meson
	dev-build/ninja
"
RDEPEND="
	app-containers/podman
	sys-apps/systemd
"
DEPEND="${RDEPEND}"

RESTRICT+="test"

PATCHES="${FILESDIR}/0001-use-local-vendor-archive.patch"

src_unpack() {
	unpack ${P}.tar.gz
	cd ${S}
	unpack ${P}-vendor.tar.gz
}

src_configure() {
	local emesonargs=(
		-Dbuildtype=plain
		-Dprofile_dir=/etc/profile.d
	)

	meson_src_configure
}

pkg_postinst() {
	tmpfiles_process toolbox.conf
}
