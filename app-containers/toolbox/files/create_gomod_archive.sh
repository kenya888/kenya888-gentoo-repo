#!/bin/env bash

PN=toolbox
PV=0.0.99.5
SRC_URI=https://github.com/containers/${PN}/archive/${PV}.tar.gz

curl -LO ${SRC_URI}

tar xvf ${PV}.tar.gz

cd ${PN}-${PV}/src
go mod vendor
cd ..
tar -zcvf ../${PN}-${PV}-vendor.tar.gz ./src/vendor

cd ..
md5sum ${PN}-${PV}-vendor.tar.gz > ${PN}-${PV}-vendor.tar.gz.md5sum

rm -rf ${PV}.tar.gz ${PN}-${PV}
